#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/user.h> // user_regs_struct

// аттачится к процессу с указанным pid
int attach(pid_t pid) {
    int status;
    
    if ((status = ptrace(PTRACE_ATTACH, pid, NULL, NULL)) < 0) {
        fprintf(stderr, "Cannot attach to PID %d: %s\n", pid, strerror(errno));
        return -1;
    }

    printf("Attached to PID %d\n", pid);

    return 0;
}

// отсоединяемся от процесса с указанным pid
int detach(pid_t pid) {
    int status;

    if ((status = ptrace(PTRACE_DETACH, pid, NULL, NULL)) < 0) {
        fprintf(stderr, "Cannot detach from PID %d: %s\n", pid, strerror(errno));
        return -1;
    }

    printf("Detached from PID %d\n", pid);

    return 0;

}

// сохраняет значения всех регистров в структуру user_regs_struct
int save_registers(pid_t pid, struct user_regs_struct *saved_regs) {
    int status;

    if ((status = ptrace(PTRACE_GETREGS, pid, NULL, saved_regs)) < 0) {
        printf("Failed\n");
        return -1;
    }

    printf("OK\n");

    return 0;
}

// восстанавливает значения всех регистров из структуры user_regs_struct
int load_registers(pid_t pid, struct user_regs_struct *saved_regs) {
    int status;

    if ((status = ptrace(PTRACE_SETREGS, pid, NULL, saved_regs)) < 0) {
        printf("Failed\n");
        return -1;
    }

    printf("OK\n");

    return 0;
}

int main(int argc, char **argv)
{
    pid_t traced_process;
    struct user_regs_struct regs;

    if (argc != 2) {
        printf("Usage:\n\t %s <PID>\n", argv[0]);

        return 1;
    }

    traced_process = atoi(argv[1]);

    if (attach(traced_process) < 0) 
        return 2;

    wait(NULL);

    printf("Saving registers... ");
    save_registers(traced_process, &regs);
    printf("Registers saved. RIP: %llx\n", regs.rip);

    printf("Putting back original instructions... ");
    load_registers(traced_process, &regs);

    if (detach(traced_process) < 0)
        return 3;

    return 0;
}

